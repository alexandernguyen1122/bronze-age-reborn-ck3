﻿throne_scone = {
	icon = "artifact_throneroom.dds"
	asset = ep1_mediterranean_throne_basic_01_a_entity
}
throne_turquoise = {
	icon = "artifact_throneroom.dds"
	asset = ep1_indian_throne_fancy_01_a_entity
}
throne_peacock = {
	icon = "artifact_throneroom.dds"
	asset = ep1_mena_throne_fancy_01_a_entity
}

olifant_court = {
	default_type = pedestal
	icon = "artifact_regalia.dds"
	asset = ep1_olifant_entity
	pedestal = "tall_pillow"
}
