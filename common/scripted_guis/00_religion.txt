﻿toggle_great_holy_war_pledge = {
	scope = character
	
	is_shown = {
		always = no
	}
	is_valid = {
		always = no
	}
	
	
	effect = {
	}
}

join_great_holy_war_directed = {
	scope = character
	
	is_shown = {
		always = no
	}
	
	effect = {
	}
}

toggle_great_holy_war_pledge_defense = {
	scope = character
	
	is_shown = {
		always = no
	}
	
	is_valid = {
		always = no
	}
	
	effect = {
	}
}

join_great_holy_war_directed_defense = {
	scope = character
	
	is_shown = {
		always = no
	}
	
	effect = {
	}
}

great_holy_war_give_gold = {
	scope = character
	
	is_shown = {
		always = no
	}
	
	is_valid = {
		always = no
	}
	
	effect = {
	}
}

toggle_great_holy_war_beneficiary_stance = {
	scope = character
	
	is_shown = {
		always = no
	}
	
	effect = {
	}
}

not_pledged_not_donated_tooltip = {
	scope = character
	is_shown = {
		always = no
	}

	is_valid = {
		always = no
	}
}

not_pledged_donated_tooltip = {
	scope = character
	is_shown = {
		always = no
	}

	is_valid = {
		always = no
	}
}

change_target = {
	scope = character
	is_shown = {
		always = no
	}

	is_valid = {
		always = no
	}
}

can_pledge_as_attacker = {
	scope = character
	
	is_shown = {
		always = no
	}
}

can_pledge_as_defender = {
	scope = character
	
	is_shown = {
		always = no
	}
}

create_head_of_faith = {
	scope = character
	saved_scopes = {
		faith
	}
	is_shown = {
		faith = scope:faith
	}
	is_valid = {
		can_create_head_of_faith_title_trigger = { FAITH = scope:faith }
	}
	effect = {
		create_head_of_faith_title_effect = yes
	}
}

recreate_head_of_faith = {
	scope = character
	saved_scopes = {
		faith
	}
	is_shown = {
		faith = scope:faith
	}
	is_valid = {
		can_create_head_of_faith_title_trigger = { FAITH = scope:faith }
	}
	effect = {
		create_head_of_faith_title_effect = yes
	}
}