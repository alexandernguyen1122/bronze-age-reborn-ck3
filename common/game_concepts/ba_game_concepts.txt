﻿settlement_holding = {
	alias = { settlement settlements settlement_holdings }
	parent = holding_type
	texture = "gfx/interface/icons/icon_holding_castle.dds"
}

agricultural_building = {
	texture = "gfx/interface/icons/message_feed/building.dds"
	parent = building
	alias = { agricultural_buildings }
}
development_decay = {
	texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { developmental_decay }
}
distance_efficiency = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { distance_inefficiency }
}
tributary = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { tributaries }
}
law_code = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { law_codes }
}
palatial = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	#alias = {  }
}
urban = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { urbans }
}
champion = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { champions }
}
cult = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { cults cult_worship }
}
deity = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { deitys deities }
}
cult_center = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { cult_centers cult_center_s }
}
cult_type = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { cult_types }
}
mistress_cult = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { mistress_cult_simple }
}
state_cult = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { state_cult_simple }
}
wisdom_cult = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { wisdom_cult_simple }
}
moon_cult = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { moon_cult_simple }
}
sun_cult = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { sun_cult_simple }
}
underworld_cult = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { underworld_cult_simple }
}
storm_cult = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { storm_cult_simple }
}
farming_cult = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { farming_cult_simple }
}
fertility_cult = {
	#texture = "gfx/interface/icons/council_task_types/task_develop_county.dds"
	alias = { fertility_cult_simple }
}
tomb_site = {
	texture = "gfx/interface/icons/icon_tombs.dds"
	alias = { tomb_sites }
}
tomb_building = {
	texture = "gfx/interface/icons/icon_tombs.dds"
	alias = { tomb_buildings }
}
tomb_type = {
	texture = "gfx/interface/icons/icon_tombs.dds"
	alias = { tomb_types }
}
entomb = {
	texture = "gfx/interface/icons/icon_tombs.dds"
	alias = { entombment entombing entombed }
}
patron = {
	alias = { patrons patronage patronage_level patronage_levels }
}
chief_patron = {
	alias = { chief_patrons }
}
splendor = {
	alias = {  }
}
cult_wealth = {
	alias = { cult_wealth_simple }
}
cult_province = {
	alias = { cultic_provinces cultic_province cult_provinces }
}
cult_practice = {
	alias = { cult_practices }
}