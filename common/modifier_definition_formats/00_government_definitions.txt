﻿feudal_government_opinion = {
	decimals = 0
}

feudal_government_vassal_opinion = {
	decimals = 0
}

feudal_government_opinion_same_faith = {
	decimals = 0
}

feudal_government_tax_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

feudal_government_tax_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

feudal_government_levy_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

feudal_government_levy_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

palatial_government_opinion = {
	decimals = 0
}

palatial_government_vassal_opinion = {
	decimals = 0
}

palatial_government_opinion_same_faith = {
	decimals = 0
}

palatial_government_tax_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

palatial_government_tax_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

palatial_government_levy_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

palatial_government_levy_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

theocracy_government_opinion = {
	decimals = 0
}

theocracy_government_vassal_opinion = {
	decimals = 0
}

theocracy_government_opinion_same_faith = {
	decimals = 0
}

theocracy_government_tax_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

theocracy_government_tax_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

theocracy_government_levy_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

theocracy_government_levy_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

tributary_government_opinion = {
	decimals = 0
}

tributary_government_vassal_opinion = {
	decimals = 0
}

tributary_government_opinion_same_faith = {
	decimals = 0
}

tributary_government_tax_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

tributary_government_tax_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

tributary_government_levy_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

tributary_government_levy_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

tribal_government_opinion = {
	decimals = 0
}

tribal_government_vassal_opinion = {
	decimals = 0
}

tribal_government_opinion_same_faith = {
	decimals = 0
}

tribal_government_tax_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

tribal_government_tax_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

tribal_government_levy_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

tribal_government_levy_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

mercenary_government_opinion = {
	decimals = 0
}

mercenary_government_vassal_opinion = {
	decimals = 0
}

mercenary_government_opinion_same_faith = {
	decimals = 0
}

mercenary_government_tax_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

mercenary_government_tax_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

mercenary_government_levy_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

mercenary_government_levy_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

holy_order_government_opinion = {
	decimals = 0
}

holy_order_government_vassal_opinion = {
	decimals = 0
}

holy_order_government_opinion_same_faith = {
	decimals = 0
}

holy_order_government_tax_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

holy_order_government_tax_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

holy_order_government_levy_contribution_add = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

holy_order_government_levy_contribution_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}
