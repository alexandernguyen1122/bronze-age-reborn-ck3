﻿proto_greek = {
	color = greek
	
	ethos = ethos_bellicose
	heritage = heritage_hellenic
	language = language_greek
	martial_custom = martial_custom_female_only #placeholder
	traditions = {
		tradition_hill_dwellers
	}
	
	name_list = name_list_proto_greek
	
	coa_gfx = { helladic_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { mycenaean_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = greek
	}
}

achaian = {
	color = hsv { 0.60  0.75  0.8 }
	
	ethos = ethos_bellicose
	heritage = heritage_hellenic
	language = language_greek
	martial_custom = martial_custom_female_only #placeholder
	traditions = {
		tradition_hill_dwellers
	}
	
	name_list = name_list_mycenaean
	
	coa_gfx = { helladic_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { mycenaean_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = greek
	}
}