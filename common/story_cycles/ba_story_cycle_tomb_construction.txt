﻿# Possible properties/variables
# tomb_building_object, tomb_site_object, tomb_real_province
# tomb_type 				pyramid, chamber tomb etc
# current/total_stages		stages needed for completion, real difference. finding location/building logistics, laying foundation, building core, decorate tombs or exterior etc
# tomb_size					tomb magnitude, small, adequate, huge, monstrous or whatever, increases needed progress to finish stage
# tomb_material				Like mudbrick or stone pyramid or something else or none if applicable
# tomb_progress_goal		100 by default but depends on stage
# delay_between_ticks		time between effect groups are fired to progress story
# chance_of_failure			lost progress
# chance_of_catastrophe		potentially ending story, pyramid tilts, structures collapse
# progress_gained			amount progress added on tick
# quality					additive multiplier to renown reward
# renown_multiplier 		multiplier to renown reward, impacted by quality, doctrines, traditions, size, events, artifacts?
# bonus_slots				entombment slots that can be given away as rewards to friends etc (like a minor title basically)
# base_renown_non_ruler		1/8th ?
# 
# Stages have different names per type and can take vastly different amounts of time. GUI shows name of next stage
# Type 				 	1st 				2nd			3rd 			4th
# Pyramid 				Location/Logistics 	Foundation 	Laying core 	Decoration
# Chamber Tomb 			Foundation 			Structure 	Decoration

ba_story_tomb_construction = {
	
	on_setup = {
		story_owner = {
			add_character_modifier = {
				modifier = financing_tomb_construction
			}
		}
		set_variable = {
			name = tomb_building_object
			value = story_owner.var:tomb_building_object
		}
		set_variable = {
			name = tomb_site_object
			value = var:tomb_building_object.var:tomb_site_object
		}
		var:tomb_site_object = {
			set_variable = {
				name = tomb_building_under_construction
				value = scope:story.var:tomb_building_object
			}
		}
		story_owner = {
			remove_variable = tomb_building_object
		}
	}

	on_end = {
		var:tomb_building_object = {
			remove_variable = tomb_progress
		}
		var:tomb_site_object = {
			remove_variable = tomb_building_under_construction
		}
		story_owner = {
			remove_character_modifier = financing_tomb_construction
		}
	}

	on_owner_death = {
		if = { 
			limit = { # Ends if no heir or heir is already busy doing another construction (rare)
				exists = story_owner.primary_heir
				NOT = { scope:story.story_owner.primary_heir = { has_character_modifier = financing_tomb_construction } }
			}
			scope:story.story_owner.primary_heir = {
				add_character_modifier = {
					modifier = financing_tomb_construction
				}
			}
			make_story_owner = story_owner.primary_heir
		}
		else = { scope:story = { end_story = yes } }
	}

	effect_group = { # Passive Progress
		days = { 30 31 }  #Duration between checks
		chance = 100	 #Chance of firing

		triggered_effect = { #Can also do first_valid effect firing
			trigger = {
			}
			effect = {
				var:tomb_building_object = {
					change_variable = {
						name = tomb_progress
						add = tomb_construction_progress_value
					}
					if = {
						limit = { var:tomb_progress >= 100 }
						change_variable = {
							name = tomb_stages
							add = 1
						}
						set_variable = { # Make it add -100 if you want progress to overflow to next stage progression
							name = tomb_progress
							value = 0
						}
					}
					if = {
						limit = { var:tomb_stages >= stages_needed_value }
						set_variable = {
							name = tomb_stages
							value = 0
						}
						scope:story.story_owner = {
							trigger_event = { id = ba_tomb.1001 }
						}
						scope:story = { end_story = yes }
					}
				}
			}
		}
	}
	effect_group = { #Initial for Large Sites
		days = { 7 21 }  #Duration between checks
		chance = 100	 #Chance of firing

		trigger = {
			var:tomb_building_object = {
				stages_needed_value >= 4
			}
		}

		triggered_effect = { #Can also do first_valid effect firing
			trigger = {
			}
			effect = {
			}
		}
	}
}
