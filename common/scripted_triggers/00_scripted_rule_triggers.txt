﻿#Controls the can_raid rule.
can_raid_trigger = {
	OR = {
		government_has_flag = government_can_raid_rule
		culture = { has_cultural_parameter = culture_can_raid_at_sea_even_if_feudal }
	}
	NOT = {
		faith = {
			has_doctrine_parameter = holy_wars_forbidden
		}
	}
}
