﻿bm_785_first_intermediate_period = {
	start_date = 785.1.1
	is_playable = yes

	character = {
		name = "bookmark_first_intermediate_waset"
		dynasty = dynn_waset1
		dynasty_splendor_level = 1
		type = male
		birth = 750.4.8
		title = d_sceptre
		government = feudal_government
		culture = upper_egyptian
		religion = egyptian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = waset1
		position = { 1350 450 }

		animation = personality_zealous
	}
	character = {
		name = "bookmark_first_intermediate_henennesut"
		dynasty = dynn_hnn1
		dynasty_splendor_level = 1
		type = male
		birth = 750.4.8
		title = k_tenth_dynasty
		government = feudal_government
		culture = upper_egyptian
		religion = egyptian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = hnn1
		position = { 650 350 }

		animation = worry
	}
	character = {
		name = "bookmark_first_intermediate_ankhtifi"
		dynasty = dynn_ankhtifi1
		dynasty_splendor_level = 1
		type = male
		birth = 755.5.12
		title = d_shrine
		government = feudal_government
		culture = upper_egyptian
		religion = egyptian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = ankhtifi1
		position = { 1275 975 }

		animation = personality_bold
	}
	character = {
		name = "bookmark_first_intermediate_kush"
		dynasty = dynn_kerma1
		dynasty_splendor_level = 1
		type = male
		birth = 749.4.20
		title = d_kerma
		government = feudal_government
		culture = nubian
		religion = kushite_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = kerma1
		position = { 600 1050 }

		animation = personality_cynical
	}
}
bm_785_great_sumerian_revolt = {
	start_date = 785.1.1
	is_playable = yes

	character = {
		name = "bookmark_sumer_uruk"
		dynasty = dynn_uruk1
		dynasty_splendor_level = 1
		type = male
		birth = 733.4.25
		title = k_sumer
		government = feudal_government
		culture = sumerian
		religion = sumerian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_HARD"
		history_id = uruk1
		position = { 550 675 }

		animation = personality_bold
	}
	character = {
		name = "bookmark_sumer_ur"
		dynasty = dynn_ur1
		dynasty_splendor_level = 1
		type = male
		birth = 758.1.3
		title = d_ur
		government = feudal_government
		culture = sumerian
		religion = sumerian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = ur1
		position = { 1000 1075 }

		animation = personality_zealous
	}
	character = { #not yet defined
		name = "bookmark_sumer_lagash"
		dynasty = dynn_lagash1
		dynasty_splendor_level = 1
		type = male
		birth = 760.3.10
		title = d_lagash
		government = feudal_government
		culture = sumerian
		religion = sumerian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = lagash1
		position = { 1450 675 }

		animation = personality_cynical
	}
	character = {
		name = "bookmark_sumer_gutium"
		dynasty = dynn_gutium1
		dynasty_splendor_level = 1
		type = male
		birth = 755.1.3
		title = k_gutium
		government = feudal_government
		culture = gutian
		religion = gutian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_IMPOSSIBLE"
		history_id = gutium1
		position = { 750 150 }

		animation = worry
	}
}
bm_1310_assassination_of_mursili = {
	start_date = 1310.1.1
	is_playable = yes

	character = {
		name = "bookmark_mursili_halab"
		dynasty = dynn_2halab1
		dynasty_splendor_level = 1
		type = male
		birth = 1272.12.21
		title = k_halab
		government = feudal_government
		culture = amoritic
		religion = amorite_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = 2halab1
		position = { 1660 950 }

		animation = personality_zealous
	}
	character = {
		name = "bookmark_mursili_qatna"
		dynasty = dynn_2qatna1
		dynasty_splendor_level = 1
		type = male
		birth = 1281.5.6
		title = k_qatna
		government = feudal_government
		culture = amoritic
		religion = amorite_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = 2qatna1
		position = { 1125 1050 }

		animation = shame
	}
	character = {
		name = "bookmark_mursili_zidanta"
		dynasty = dynn_2hattusa1
		dynasty_splendor_level = 1
		type = male
		birth = 1283.1.3
		title = d_washaniya
		government = feudal_government
		culture = nesili
		religion = hittite_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = 2zidanta1
		position = { 550 200 }

		animation = personality_cynical
	}
	character = {
		name = "bookmark_mursili_hantili"
		dynasty = dynn_2hattusa1
		dynasty_splendor_level = 1
		type = male
		birth = 1272.2.4
		title = e_hattusa
		government = feudal_government
		culture = nesili
		religion = hittite_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 2hattusa1
		position = { 1425 125 }

		animation = personality_greedy
	}
	character = {
		name = "bookmark_mursili_mitanni"
		dynasty = dynn_2keret1
		dynasty_splendor_level = 1
		type = male
		birth = 1274.5.5
		title = k_khabur
		government = feudal_government
		culture = haburi
		religion = western_hurrian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = 2keret1
		position = { 1650 425 }

		animation = personality_bold
	}
	character = {
		name = "bookmark_mursili_cyprus"
		dynasty = dynn_2alashiya1
		dynasty_splendor_level = 1
		type = male
		birth = 1287.8.11
		title = k_alasiya
		government = feudal_government
		culture = cypriot
		religion = cypriot_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 2alashiya1
		position = { 350 1000 }

		animation = personality_cynical
	}
	character = {
		name = "bookmark_mursili_alalakh"
		dynasty = dynn_2halab1 #house_alalakh_yamhad
		dynasty_splendor_level = 1
		type = male
		birth = 1294.4.12
		title = k_mukis
		government = feudal_government
		culture = amoritic
		religion = amorite_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_HARD"
		history_id = 2alalakh1
		position = { 725 1075 }

		animation = worry
	}
	character = {
		name = "bookmark_mursili_kizzuwatna"
		dynasty = dynn_2kizzuwatna1
		dynasty_splendor_level = 1
		type = male
		birth = 1275.6.13
		title = k_kizzuwatna
		government = tributary_government
		culture = kizzuni
		religion = western_hurrian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 2kizzuwatna1
		position = { 625 625 }

		animation = dismissal
	}
}
bm_1310_sack_of_babylon = {
	start_date = 1310.1.1
	is_playable = yes

	character = { #Not yet specified age name etc
		name = "bookmark_babylonia_kassites"
		dynasty = dynn_2kassite1
		dynasty_splendor_level = 1
		type = male
		birth = 1278.1.24
		title = k_babylon
		government = feudal_government
		culture = kassite
		religion = kassite_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 2kassite1
		position = { 850 900 }

		animation = personality_bold
	}
	character = { #Not yet specified age name etc
		name = "bookmark_babylonia_sealand"
		dynasty = dynn_2sumer1
		dynasty_splendor_level = 1
		type = male
		birth = 1293.9.12
		title = k_sumer
		government = feudal_government
		culture = neo_sumerian
		religion = sumerian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = 2sumer1
		position = { 1750 850 }

		animation = personality_honorable
	}
	character = { #Not yet specified age name etc
		name = "bookmark_babylonia_khana"
		dynasty = dynn_2khana1
		dynasty_splendor_level = 1
		type = male
		birth = 1277.9.22
		title = k_mari
		government = feudal_government
		culture = amoritic
		religion = amorite_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = 2khana1
		position = { 550 175 }

		animation = worry
	}
	character = { 
		name = "bookmark_babylonia_assyria"
		dynasty = dynn_2assyria1
		dynasty_splendor_level = 1
		type = male
		birth = 1285.2.24
		title = k_assyria
		government = feudal_government
		culture = assyrian
		religion = akkadian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 2assyria1
		position = { 1450 400 }

		animation = personality_honorable
	}
}
bm_1310_second_intermediate_period = {
	start_date = 1310.1.1
	is_playable = yes

	character = { #Not yet specified age name etc
		name = "bookmark_second_intermediate_waset"
		dynasty = dynn_2waset1
		dynasty_splendor_level = 1
		type = male
		birth = 1264.3.7
		title = k_upper_egypt
		government = feudal_government
		culture = upper_egyptian
		religion = egyptian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_HARD"
		history_id = 2waset1
		position = { 1450 420 }

		animation = personality_rational
	}
	character = { #Not yet specified age name etc
		name = "bookmark_second_intermediate_hyksos"
		dynasty = dynn_2hyksos1
		dynasty_splendor_level = 1
		type = male
		birth = 1292.2.4
		title = k_lower_egypt
		government = feudal_government
		culture = hyksos
		religion = hyksos_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 2hyksos1
		position = { 625 350 }

		animation = boredom
	}
	character = { #Not yet specified age name etc
		name = "bookmark_second_intermediate_kush"
		dynasty = dynn_2kush1
		dynasty_splendor_level = 1
		type = male
		birth = 1274.2.26
		title = k_kush
		government = feudal_government
		culture = nubian
		religion = kushite_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 2kush1
		position = { 700 950 }

		animation = personality_forgiving
	}
}
bm_1310_eruption_of_thera = {
	start_date = 1310.1.1
	is_playable = yes

	character = { #Not yet specified age name etc
		name = "bookmark_thera_knossos"
		dynasty = dynn_2knossos1
		dynasty_splendor_level = 1
		type = male
		birth = 1290.4.25
		title = k_minoa
		government = feudal_government
		culture = minoan
		religion = minoan_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_EASY"
		history_id = 2knossos1
		position = { 1300 850 }

		animation = disbelief
	}
	character = { #Not yet specified age name etc
		name = "bookmark_thera_mycenaea"
		dynasty = dynn_2mykene1
		dynasty_splendor_level = 1
		type = male
		birth = 1272.2.8
		title = k_argolis
		government = feudal_government
		culture = achaian
		religion = mycenean_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = 2mykene1
		position = { 800 300 }

		animation = personality_bold
	}
	character = { #Not yet specified age name etc
		name = "bookmark_thera_troy"
		dynasty = dynn_2troy1
		dynasty_splendor_level = 1
		type = male
		birth = 1278.7.17
		title = d_wilion
		government = feudal_government
		culture = wilusan
		religion = anatolian_religion
		difficulty = "BOOKMARK_CHARACTER_DIFFICULTY_MEDIUM"
		history_id = 2troy1
		position = { 1650 250 }

		animation = personality_greedy
	}
}