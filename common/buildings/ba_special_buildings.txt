﻿#Locational/Geopgraphical
island_of_tyre_01 = {

	construction_time = slow_construction_time

	type_icon = "icon_structure_doges_palace.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000
	
	county_modifier = {
		tax_mult = 0.3
		development_growth = 0.5
		development_growth_factor = 0.2
	}
	province_modifier = {
		fort_level = 3
		hostile_raid_time = 0.5
	}
	character_modifier = {
		monthly_stewardship_lifestyle_xp_gain_mult = 0.05
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
island_of_arwad_01 = {

	construction_time = slow_construction_time

	type_icon = "icon_structure_doges_palace.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000
	
	county_modifier = {
		tax_mult = 0.3
		development_growth = 0.5
		development_growth_factor = 0.2
	}
	province_modifier = {
		fort_level = 3
		hostile_raid_time = 0.5
	}
	character_modifier = {
		monthly_stewardship_lifestyle_xp_gain_mult = 0.05
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
jericho_spring_01 = {

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_pyramids.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 600

	county_modifier = {
		development_growth = 0.2
	}
	
	province_modifier = {
		supply_limit_mult = 0.25
		supply_limit = 500
	}

	ai_value = {
		base = 100
	}
	
	type = special
}
barada_spring_01 = {

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_pyramids.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 600

	county_modifier = {
		development_growth = 0.2
	}
	
	province_modifier = {
		supply_limit_mult = 0.25
		supply_limit = 500
	}
	
	
	ai_value = {
		base = 100
	}
	
	type = special
}

#Other
ruins_of_halab_01 = {
	construction_time = very_slow_construction_time
	type_icon = "icon_structure_alhambra.dds"
	
	can_construct = { }
	
	cost_gold = 2000

	character_modifier = { }
	province_modifier = { }
	county_modifier = { 
		tax_mult = -0.2
		development_growth_factor = -0.25
	}
	
	ai_value = {
		base = 100
	}

	next_building = halab_01
	
	type = special
}
halab_01 = { #Temple of the Storm God
	construction_time = very_slow_construction_time
	type_icon = "icon_structure_alhambra.dds"
	
	can_construct = { }
	
	cost_gold = 750

	character_modifier = {
		monthly_piety_gain_mult = 0.1
	}
	province_modifier = { }
	county_modifier = {
		tax_mult = 0.1
		development_growth = 0.2
	}
	
	ai_value = {
		base = 100
	}

	next_building = rebuilt_halab_02
	
	type = special
}
rebuilt_halab_02 = {
	construction_time = very_slow_construction_time
	type_icon = "icon_structure_alhambra.dds"
	
	can_construct = { }
	
	cost_gold = 750

	character_modifier = {
		monthly_piety_gain_mult = 0.1
		monthly_learning_lifestyle_xp_gain_mult = 0.05
		learning_per_piety_level = 2
	}
	province_modifier = {
		monthly_income = 0.5
	}
	county_modifier = {
		tax_mult = 0.25
		development_growth = 0.5
	}
	
	ai_value = {
		base = 100
	}

	type = special
}
ruined_esagila_01 = {
	construction_time = very_slow_construction_time
	type_icon = "icon_structure_great_mosque_of_djenne.dds"
	
	cost_gold = 2000

	character_modifier = { 
		monthly_piety_gain_mult = -0.1
		monthly_prestige_gain_mult = -0.1
	}
	province_modifier = { }
	county_modifier = { }
	
	ai_value = {
		base = 100
	}

	next_building = esagila_01
	
	type = special
}
esagila_01 = {
	construction_time = very_slow_construction_time
	type_icon = "icon_structure_great_mosque_of_djenne.dds"
	
	can_construct = { }
	
	cost_gold = 750

	character_modifier = { 
		monthly_piety_gain_mult = 0.05
		monthly_prestige_gain_mult = 0.05
		direct_vassal_opinion = 3
	}
	province_modifier = {
		monthly_income = 1
	}
	county_modifier = {
		tax_mult = 0.1
		development_growth = 0.4
		development_growth_factor = 0.25
	}
	
	ai_value = {
		base = 100
	}

	next_building = esagila_02
	
	type = special
}
esagila_02 = {
	
	can_construct = { }
	
	cost_gold = 750

	character_modifier = { 
		monthly_piety_gain_mult = 0.1
		monthly_prestige_gain_mult = 0.1
		direct_vassal_opinion = 5
	}
	province_modifier = {
		monthly_income = 2
	}
	county_modifier = {
		tax_mult = 0.2
		development_growth = 0.8
		development_growth_factor = 0.4
	}
	
	ai_value = {
		base = 100
	}

	type = special
}
oracle_of_wadjet_01 = {

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_pyramids.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000

	character_modifier = {
		monthly_dynasty_prestige_mult = 0.05
		learning_per_piety_level = 1
		diplomatic_range_mult = 0.25
	}
	county_modifier = {
		development_growth = 0.2
		development_growth_factor = 0.25
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
pyramids_of_giza_01 = {

	asset = {
		type = pdxmesh
		name = "building_special_pyramids_giza_mesh"
	}

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_pyramids.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 3000

	character_modifier = {
		monthly_dynasty_prestige_mult = 0.05
		short_reign_duration_mult = -0.2
	}
	county_modifier = {
		development_growth = 0.1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
restored_ekur_temple_01 = {
	type_icon = "icon_structure_great_mosque_of_djenne.dds"
	construction_time = very_slow_construction_time

	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 750

	character_modifier = { 
		monthly_piety_gain_mult = 0.1
	}
	province_modifier = {
	}
	county_modifier = {
		tax_mult = 0.1
		development_growth = 0.2
		development_growth_factor = 0.15
	}
	
	ai_value = {
		base = 100
	}
	
	next_building = restored_ekur_temple_02
	
	type = special
}

restored_ekur_temple_02 = {
	construction_time = very_slow_construction_time

	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 1000

	character_modifier = { 
		monthly_piety_gain_mult = 0.1
		monthly_dynasty_prestige_mult = 0.05
	}
	province_modifier = {
	}
	county_modifier = {
		tax_mult = 0.2
		development_growth = 0.4
		development_growth_factor = 0.25
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

house_of_waters_01 = {

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_pyramids.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 1000

	character_modifier = {
		health = 0.25
	}
	county_modifier = {
		development_growth = 0.2
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

eshumesha_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_aurelian_walls.dds"
	
	can_construct_potential = {
		barony = title:b_6000
		building_requirement_tribal = no
	}
	
	cost_gold = 750

	character_modifier = {
		monthly_piety_gain_mult = 0.05
		monthly_learning_lifestyle_xp_gain_mult = 0.05
	}
	duchy_capital_county_modifier = {
		tax_mult = 0.15
	}
	county_modifier = {
		development_growth = 0.2
		development_growth_factor = 0.1
	}
	
	ai_value = {
		base = 100
	}
	
	next_building = eshumesha_02
	
	type = duchy_capital
}
eshumesha_02 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_aurelian_walls.dds"
	
	can_construct_potential = {
		barony = title:b_6000
		building_requirement_tribal = no
	}
	
	cost_gold = 750
	
	character_modifier = {
		monthly_piety_gain_mult = 0.1
		monthly_learning_lifestyle_xp_gain_mult = 0.1
	}
	duchy_capital_county_modifier = {
		tax_mult = 0.25
	}
	county_modifier = {
		development_growth = 0.5
		development_growth_factor = 0.25
	}
	
	ai_value = {
		base = 100
	}
	
	type = duchy_capital
}
uruk_walls_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_aurelian_walls.dds"
	
	can_construct_potential = {
		barony = title:b_6022
		building_requirement_tribal = no
	}
	
	cost_gold = 3000
	
	max_garrison = 500
	
	duchy_capital_county_modifier = {
		levy_size = 0.2
		tax_mult = 0.2
	}
	county_modifier = {
		development_growth = 0.8
		development_growth_factor = 0.4
		hostile_raid_time = 1
	}
	province_modifier = {
		fort_level = 5
		garrison_size = 0.5
	}
	
	ai_value = {
		base = 100
	}
	
	type = duchy_capital
}
eanna_temple_01 = {

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_grand_library_of_baghdad.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 600

	character_modifier = {
		negate_fertility_penalty_add = 0.2
		stress_gain_mult = -0.1
	}
	county_modifier = {
		development_growth = 0.2
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}

generic_pyramid_01 = {

	asset = {
		type = pdxmesh
		name = "building_special_pyramids_giza_mesh"
	}

	construction_time = very_slow_construction_time

	type_icon = "icon_structure_the_pyramids.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 600

	county_modifier = {
		development_growth = 0.1
	}
	character_modifier = {
		monthly_dynasty_prestige_mult = 0.05
		short_reign_duration_mult = -0.2
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
mykene_walls_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_aurelian_walls.dds"
	
	can_construct = {
		scope:holder = { culture = { has_innovation = innovation_cyclopean_walls } }
	}

	cost_gold = 1500
	
	max_garrison = 500
	
	county_modifier = {
		development_growth = 0.5
		development_growth_factor = 0.25
		hostile_raid_time = 1
		levy_size = 0.25
		tax_mult = 0.25
	}
	province_modifier = {
		fort_level = 5
		garrison_size = 0.5
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
trojan_walls_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_structure_aurelian_walls.dds"
	
	can_construct = {
		scope:holder = { culture = { has_innovation = innovation_fortifications_3 } }
	}

	cost_gold = 1500
	
	max_garrison = 500
	
	county_modifier = {
		development_growth = 0.5
		development_growth_factor = 0.25
		hostile_raid_time = 1
		levy_size = 0.25
		tax_mult = 0.25
	}
	province_modifier = {
		fort_level = 5
		garrison_size = 0.5
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
great_ziggurat_01 = {

	construction_time = slow_construction_time

	type_icon = "icon_structure_mahabodhi_temple.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 1500
	
	county_modifier = {
		tax_mult = 0.25
		development_growth = 0.5
		development_growth_factor = 0.2
	}
	character_modifier = {
		monthly_piety_gain_mult = 0.1
		monthly_dynasty_prestige_mult = 0.05
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
kanesh_karum_01 = {

	construction_time = slow_construction_time

	type_icon = "icon_structure_university_of_siena.dds"

	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 1500
	
	county_modifier = {
		tax_mult = 0.25
		development_growth = 0.5
		development_growth_factor = 0.2
	}
	province_modifier = {
		monthly_income = 2
	}
	character_modifier = {
		monthly_stewardship_lifestyle_xp_gain_mult = 0.05
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
ba_petra_01 = {

	asset = {
		type = pdxmesh
		name = "building_special_petra_mesh"
	}
	construction_time = slow_construction_time

	type_icon = "icon_structure_petra.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 1500
	
	county_modifier = {
		tax_mult = 0.2
		development_growth = 0.4
		supply_limit_mult = 0.25
		development_growth_factor = 0.2
	}
	province_modifier = {
		monthly_income = 2
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
sub_saharan_imports_01 = {

	construction_time = slow_construction_time

	type_icon = "icon_building_ivory.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	cost_gold = 1500
	
	county_modifier = {
		development_growth = 0.3
		development_growth_factor = 0.15
	}
	province_modifier = {
		monthly_income = 1
	}
	
	ai_value = {
		base = 100
	}
	
	type = special
}
# Ideas
# Resource Based:
# Hit: Bitumen Mine
# Cyprus: Copper Mines
# Silver Mountain: Mines
# Mining Expeditions in Egypt and Nubia
# 
#
#
#
# Normal
# Kanesh: Assyrian Korum
# West of Waset: Hathshepshut Mortuary Temple (Buildable, built in 1600 bce start)
# Ur: Great Ziggurat
# Petra: Buildable
# Giza: Pyramid
# Mykene: Great Walls (Buildable)
# Knossos: Minos Labyrinth (Buildable)
# Troy: Great Walls of Troy (Buildable)
# Tyre: Island of Tyre
# Arwad: Island of Arwad
# Nippur: Great Temple (One for Duchy building too)
# Uruk: Inanna Temple (?)
# Assur: Ishtar Gate
# Per-Wadjet: Oracle of Wadjet
# Delphi: Oracle of Delphi
# Dodona: Oracle of Dodona
# More Greek Oracles: Athens, Olympia, Delos etc
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#Skeleton
_01 = {
	construction_time = very_slow_construction_time
	type_icon = "icon_structure_alhambra.dds"
	
	can_construct = { }
	
	cost_gold = 2000

	character_modifier = { }
	province_modifier = { }
	county_modifier = { }
	
	ai_value = {
		base = 100
	}

	next_building = _02
	
	type = special
}
_02 = {
	construction_time = very_slow_construction_time
	type_icon = "icon_structure_alhambra.dds"
	
	can_construct = { }
	
	cost_gold = 2000

	character_modifier = { }
	province_modifier = { }
	county_modifier = { }
	
	ai_value = {
		base = 100
	}

	type = special
}