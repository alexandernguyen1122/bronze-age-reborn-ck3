﻿@holding_illustration_western = "gfx/interface/illustrations/holding_types/city_western.dds"
@holding_illustration_india = "gfx/interface/illustrations/holding_types/city_india.dds"
@holding_illustration_mediterranean = "gfx/interface/illustrations/holding_types/city_mediterranean.dds"
@holding_illustration_mena = "gfx/interface/illustrations/holding_types/city_mena.dds"

settlement_01 = {
	construction_time = slow_construction_time
	
	asset = {
		type = pdxmesh
		names = {
			"western_city_01_a_mesh"
			"western_city_01_b_mesh"
			"western_city_01_c_mesh"
		}
		illustration = @holding_illustration_western
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/western_city" soundparameter = { "Tier" = 1 } }
	}
	
	asset = {
		type = pdxmesh
		names = {
			"building_india_city_01_mesh"
		}
		illustration = @holding_illustration_mena
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/mena_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_mena }
	}
	
	asset = {
		type = pdxmesh
		names = {
			"building_india_city_01_mesh"
		}
		illustration = @holding_illustration_india
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/indian_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_india }
	}
	
	asset = {
		type = pdxmesh
		name = "building_mediterranean_city_01_mesh"
		illustration = @holding_illustration_mediterranean
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/mediterranean_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_mediterranean }
	}

	can_construct_potential = {
		building_requirement_tribal = no
		barony_cannot_construct_holding = no
	}
	
	can_construct = {
		culture = {
			simple_agricultural_building_requirement_tier_1 = yes
		}
	}
	
	can_construct_showing_failures_only = {
		building_requirement_tribal_holding_in_county = no
	}
	cost_gold = main_building_tier_1_cost
	
	levy = 125
	max_garrison = normal_building_max_garrison_tier_1
	province_modifier = {
		monthly_income = 0.2
	}
	county_modifier = {
		development_growth = 0.2
	}
	
	flag = settlement
	next_building = settlement_02

	type_icon = "icon_building_guild_halls.dds"
	
	ai_value = {
		base = 0
		modifier = {
			add = 100
			scope:holder = {
				domain_limit_available < 1
			}
		}		
		modifier = {
			factor = 0
			scope:holder = {
				has_government = theocracy_government
			}
		}
	}
}

settlement_02 = {
	construction_time = slow_construction_time
	
	asset = {
		type = pdxmesh
		names = {
			"western_city_01_a_mesh"
			"western_city_01_b_mesh"
			"western_city_01_c_mesh"
		}
		illustration = @holding_illustration_western
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/western_city" soundparameter = { "Tier" = 1 } }
	}
	
	asset = {
		type = pdxmesh
		names = {
			"building_mena_city_01_mesh"
		}
		illustration = @holding_illustration_mena
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/mena_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_mena }
	}
	
	asset = {
		type = pdxmesh
		names = {
			"building_india_city_01_mesh"
		}
		illustration = @holding_illustration_india
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/indian_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_india }
	}
	
	asset = {
		type = pdxmesh
		name = "building_mediterranean_city_01_mesh"
		illustration = @holding_illustration_mediterranean
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/mediterranean_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_mediterranean }
	}

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		culture = {
			simple_agricultural_building_requirement_tier_2 = yes
		}
	}
	cost_gold = main_building_tier_2_cost
	
	levy = 200
	max_garrison = normal_building_max_garrison_tier_2
	province_modifier = {
		monthly_income = 0.4
	}
	county_modifier = {
		development_growth = 0.3
	}
	flag = settlement
	next_building = settlement_03
	ai_value = {
		base = 6		
		modifier = {
			factor = 2
			scope:holder.capital_province = this
		}
	}
}

settlement_03 = {
	construction_time = slow_construction_time
	
	asset = {
		type = pdxmesh
		names = {
			"western_city_01_a_mesh"
			"western_city_01_b_mesh"
			"western_city_01_c_mesh"
		}
		illustration = @holding_illustration_western
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/western_city" soundparameter = { "Tier" = 1 } }
	}
	
	asset = {
		type = pdxmesh
		names = {
			"building_mena_city_01_mesh"
		}
		illustration = @holding_illustration_mena
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/mena_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_mena }
	}
	
	asset = {
		type = pdxmesh
		names = {
			"building_india_city_01_mesh"
		}
		illustration = @holding_illustration_india
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/indian_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_india }
	}
	
	asset = {
		type = pdxmesh
		name = "building_mediterranean_city_01_mesh"
		illustration = @holding_illustration_mediterranean
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/mediterranean_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_mediterranean }
	}

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		culture = {
			simple_agricultural_building_requirement_tier_3 = yes
		}
	}
	cost_gold = main_building_tier_3_cost
	next_building = settlement_04
	
	levy = 300
	max_garrison = normal_building_max_garrison_tier_3
	province_modifier = {
		monthly_income = 0.6
	}
	county_modifier = {
		development_growth = 0.4
	}
	flag = settlement
	ai_value = {
		base = 5		
		modifier = {
			factor = 2
			scope:holder.capital_province = this
		}
	}
}

settlement_04 = {
	construction_time = slow_construction_time
	
	asset = {
		type = pdxmesh
		names = {
			"western_city_01_a_mesh"
			"western_city_01_b_mesh"
			"western_city_01_c_mesh"
		}
		illustration = @holding_illustration_western
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/western_city" soundparameter = { "Tier" = 1 } }
	}
	
	asset = {
		type = pdxmesh
		names = {
			"building_mena_city_01_mesh"
		}
		illustration = @holding_illustration_mena
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/mena_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_mena }
	}
	
	asset = {
		type = pdxmesh
		names = {
			"building_india_city_01_mesh"
		}
		illustration = @holding_illustration_india
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/indian_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_india }
	}
	
	asset = {
		type = pdxmesh
		name = "building_mediterranean_city_01_mesh"
		illustration = @holding_illustration_mediterranean
		soundeffect = { soundeffect = "event:/SFX/Ambience/3DMapEmitters/Holdings/City/mediterranean_city" soundparameter = { "Tier" = 1 } }
		graphical_regions = { graphical_mediterranean }
	}

	can_construct_potential = {
		building_requirement_tribal = no
	}
	can_construct = {
		culture = {
			simple_agricultural_building_requirement_tier_4 = yes
		}
	}
	cost_gold = main_building_tier_4_cost
	
	levy = 400
	max_garrison = normal_building_max_garrison_tier_7
	province_modifier = {
		monthly_income = 0.8
	}
	county_modifier = {
		development_growth = 0.5
	}
	flag = settlement
	ai_value = {
		base = 4		
		modifier = {
			factor = 2
			scope:holder.capital_province = this
		}
	}
}


####
#
# City Buildings
#
####

### Guild Halls
