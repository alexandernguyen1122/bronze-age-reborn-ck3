﻿entomb_character_effect = { # Root and scope is Character that died
	if = {
		limit = {
			should_be_entombed_trigger = yes
		}
		save_scope_as = player #Because that is what the scope is called in script_value
		if = {
			limit = { house = { has_variable = primary_tomb_site } }
			house.var:primary_tomb_site = {
				try_entomb_effect = yes
			}
		}
		every_in_list = {
			variable = tomb_site_objects
			limit = {
				ROOT = { NOT = { has_character_flag = is_entombed } }
				has_variable_list = tomb_site_buildings
			}
			every_in_list = {
				variable = tomb_site_buildings
				limit = {
					ROOT = { NOT = { has_character_flag = is_entombed } }
					has_variable = tomb_building_finished
				}
				try_entomb_effect = yes
			}
		}
	}
}
try_entomb_effect = { #SCOPE IS PROVINCE (tomb site object), root is character
	if = {
		limit = { available_slots_value >= 1 }
		add_character_to_tomb_effect = {
			CHAR = root
		}
	}
	#Nothing happens and it tries any other sites
}
toggle_character_disallow_burial_effect = { # HOUSE, CHARACTER
	$HOUSE$ = {
		if = {
			limit = { 
				is_target_in_variable_list = { 
					name = disallowed_characters_tomb 
					target = $CHARACTER$ 
				}
			}
			remove_list_variable = {
				name = disallowed_characters_tomb
				target = $CHARACTER$
			}
			$CHARACTER$ = {
				remove_variable = allowed_tomb
				remove_variable = disallowed_tomb
			}
		}
		else = {
			add_to_variable_list = {
				name = disallowed_characters_tomb
				target = $CHARACTER$
			}
			$CHARACTER$ = {
				set_variable = {
					name = disallowed_tomb
					target = $HOUSE$
				}
			}
		}
	}
}
toggle_character_allow_burial_effect = { # args: HOUSE, CHARACTER
	$HOUSE$ = {
		if = {
			limit = { 
				is_target_in_variable_list = { 
					name = allowed_characters_tomb 
					target = $CHARACTER$ 
				}
			}
			remove_list_variable = {
				name = allowed_characters_tomb
				target = $CHARACTER$
			}
			$CHARACTER$ = {
				remove_variable = allowed_tomb
				remove_variable = disallowed_tomb
			}
		}
		else = {
			add_to_variable_list = {
				name = allowed_characters_tomb
				target = $CHARACTER$
			}
			$CHARACTER$ = {
				set_variable = {
					name = allowed_tomb
					target = $HOUSE$
				}
			}
		}
	}
}
remove_character_allow_burial_effect = { # args: HOUSE, CHARACTER
	$HOUSE$ = {
		remove_list_variable = {
			name = disallowed_characters_tomb
			target = $CHARACTER$
		}
	}
	$CHARACTER$ = {
		remove_variable = disallowed_tomb
	}
}
add_character_allow_burial_effect = { # args: HOUSE, CHARACTER
	$HOUSE$ = {
		add_to_variable_list = {
			name = allowed_characters_tomb
			target = $CHARACTER$
		}
	}
	$CHARACTER$ = {
		if = {
			limit = { has_variable = disallowed_tomb }
			remove_variable = disallowed_tomb
		}
		set_variable = {
			name = allowed_tomb
			target = $HOUSE$
		}
	}
}
remove_character_disallow_burial_effect = { # args: HOUSE, CHARACTER
	$HOUSE$ = {
		remove_list_variable = {
			name = disallowed_characters_tomb
			target = $CHARACTER$
		}
	}
	$CHARACTER$ = {
		remove_variable = disallowed_tomb
	}
}
add_character_disallow_burial_effect = { # args: HOUSE, CHARACTER
	$HOUSE$ = {
		add_to_variable_list = {
			name = disallowed_characters_tomb
			target = $CHARACTER$
		}
	}
	$CHARACTER$ = {
		if = {
			limit = { has_variable = allowed_tomb }
			remove_variable = allowed_tomb
		}
		set_variable = {
			name = disallowed_tomb
			target = $HOUSE$
		}
	}
}
designate_character_for_tomb_site_effect = { # CHARACTER
	set_variable = {
		name = designated_character
		value = $CHARACTER$
	}
}
add_character_to_tomb_effect = { # SCOPE IS PROVINCE (tomb site object)
	add_to_variable_list = {
		name = entombed_characters
		target = $CHAR$
	}
	$CHAR$ = { add_character_flag = is_entombed }
}
finalize_add_tomb_building_effect = { #SCOPE IS PROVINCE (tomb building object), args: DURATION
	var:tomb_real_province = { remove_province_modifier = tomb_construction_in_progress }
	set_variable = {
		name = tomb_building_finished
		value = 1
	}
	var:tomb_site_object = {
		remove_variable = tomb_building_under_construction
	}
}
start_add_tomb_building_effect = { # SCOPE IS PROVINCE (tomb site object) args: $FOUNDING_CHARACTER$
	save_scope_as = tomb_site_object
	# Global list shenanigans
	random_in_global_list = {
		variable = unused_tomb_buildings

		save_scope_as = tomb_building_object
	}
	remove_list_global_variable = {
		name = unused_tomb_buildings
		target = scope:tomb_building_object
	}
	add_to_variable_list = {
		name = tomb_building_objects
		target = scope:tomb_building_object
	}
	scope:tomb_building_object = {
		set_variable = {
			name = tomb_site_object
			value = scope:tomb_site_object
		}
		set_variable = {
			name = tomb_real_province
			value = scope:tomb_site_object.var:tomb_real_province
		}
		set_variable = {
			name = tomb_size
			value = 0
		}
		set_variable = {
			name = tomb_progress
			value = 0
		}
		set_variable = {
			name = tomb_stages
			value = 0
		}
		if = {
			limit = { scope:tomb_site_object.var:tomb_house.house_head.faith = { has_doctrine = doctrine_tomb_type_chamber_tomb } }
			set_variable = {
				name = tomb_type
				value = 0
			}
		}
		else_if = {
			limit = { scope:tomb_site_object.var:tomb_house.house_head.faith = { has_doctrine = doctrine_tomb_type_pyramids } }
			set_variable = {
				name = tomb_type
				value = 1
			}
		}
		else_if = {
			limit = { scope:tomb_site_object.var:tomb_house.house_head.faith = { has_doctrine = doctrine_tomb_type_earth_mound } }
			set_variable = {
				name = tomb_type
				value = 2
			}
		}
		else_if = {
			limit = { scope:tomb_site_object.var:tomb_house.house_head.faith = { has_doctrine = doctrine_tomb_type_cist_tomb } }
			set_variable = {
				name = tomb_type
				value = 3
			}
		}
	}
}
pay_add_tomb_site_effect = {
	#Pay for adding a tomb
}
set_primary_tomb_site_effect = { # Scope is house, args: PROV
	set_variable = {
		name = primary_tomb_site
		value = $PROV$
	}
}
get_tomb_type_name_effect = { #SCOPE Province, the tomb site object
	if = {
		limit = { var:tomb_type = 0 }
		custom_description_no_bullet = {
			text = "chamber_tomb_name"
		}
	}
	else_if = {
		limit = { var:tomb_type = 1 }
		custom_description_no_bullet = {
			text = pyramid_name
		}
	}
	else_if = {
		limit = { var:tomb_type = 2 }
		custom_description_no_bullet = {
			text = "earth_mound_name"
		}
	}
	else_if = {
		limit = { var:tomb_type = 3 }
		custom_description_no_bullet = {
			text = "cist_tomb_name"
		}
	}
}
add_tomb_site_effect = { #SCOPE is the real Province, args: FOUNDING_CHARACTER
	save_scope_as = tomb_province
	# Global list shenanigans
	random_in_global_list = {
		variable = unused_tomb_sites

		save_scope_as = tomb_site_object
	}
	remove_list_global_variable = {
		name = unused_tomb_sites
		target = scope:tomb_site_object
	}
	add_to_global_variable_list = {
		name = global_tomb_sites
		target = scope:tomb_site_object
	}
	# Set links between objects and initial values
	add_to_variable_list = {
		name = dynasties_with_tombs
		target = $FOUNDING_CHARACTER$.House
	}
	add_to_variable_list = {
		name = tomb_site_objects
		target = scope:tomb_site_object
	}
	$FOUNDING_CHARACTER$ = {
		add_character_flag = founded_tomb_site
		house = {
			add_to_variable_list = {
				name = tomb_site_objects
				target = scope:tomb_site_object
			}
		}
	}
	scope:tomb_site_object = {
		set_variable = {
			name = tomb_real_province
			value = scope:tomb_province
		}
		set_variable = {
			name = tomb_house
			value = $FOUNDING_CHARACTER$.House
		}
	}
}