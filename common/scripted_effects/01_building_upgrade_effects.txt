﻿##################################################
# Decisions
##################################################

# Upgrades a single farm type building
# If such building does not exist, it is built. If building does not exist and there is no slot, it is built and a slot is added.
upgrade_farm_building = {
	if = {
		limit = { 
			building_cereal_fields_requirement_terrain = yes
			NOT = { has_holding_type = city_holding }
		}
		if = {
			limit = {
				NOT = { has_building = cereal_fields_01 }
				free_building_slots = 0
			}
			add_building_slot = 1
			add_building = cereal_fields_01
		}
		else_if = {
			limit = {
				NOT = { has_building = cereal_fields_01 }
				free_building_slots > 0
			}
			add_building = cereal_fields_01
		}
		else_if = {
			limit = {
				has_building = cereal_fields_01
			}
			add_building = cereal_fields_02
		}
		else_if = {
			limit = {
				has_building = cereal_fields_02
			}
			add_building = cereal_fields_03
		}
		else_if = {
			limit = {
				has_building = cereal_fields_03
			}
			add_building = cereal_fields_04
		}
		else_if = {
			limit = {
				has_building = cereal_fields_04
			}
			add_building = cereal_fields_05
		}
		else_if = {
			limit = {
				has_building = cereal_fields_05
			}
			add_building = cereal_fields_06
		}
		else_if = {
			limit = {
				has_building = cereal_fields_06
			}
			add_building = cereal_fields_07
		}
		else_if = {
			limit = {
				has_building = cereal_fields_07
			}
			add_building = cereal_fields_08
		}
	}
	else_if = {
		limit = {
			building_pastures_requirement_terrain = yes
			NOT = { has_holding_type = city_holding }
		}
		if = {
			limit = {
				NOT = { has_building = pastures_01 }
				free_building_slots = 0
			}
			add_building_slot = 1
			add_building = pastures_01
		}
		else_if = {
			limit = {
				NOT = { has_building = pastures_01 }
				free_building_slots >= 0
			}
			add_building = pastures_01
		}
		else_if = {
			limit = {
				has_building = pastures_01
			}
			add_building = pastures_02
		}
		else_if = {
			limit = {
				has_building = pastures_02
			}
			add_building = pastures_03
		}
		else_if = {
			limit = {
				has_building = pastures_03
			}
			add_building = pastures_04
		}
		else_if = {
			limit = {
				has_building = pastures_04
			}
			add_building = pastures_05
		}
		else_if = {
			limit = {
				has_building = pastures_05
			}
			add_building = pastures_06
		}
		else_if = {
			limit = {
				has_building = pastures_06
			}
			add_building = pastures_07
		}
		else_if = {
			limit = {
				has_building = pastures_07
			}
			add_building = pastures_08
		}
	}
}

# Upgrades a single tradeport building
# If such a building does not exist and can be built, it is built. If no slot exists, a slot is added and it is built.
upgrade_tradeport = {
	if = {
		limit = {
			NOR = {
				has_building = common_tradeport_01
				has_building = common_tradeport_02
				has_building = common_tradeport_03
				has_building = common_tradeport_04
				has_building = common_tradeport_05
				has_building = common_tradeport_06
				has_building = common_tradeport_07
				has_building = common_tradeport_08
			}
			free_building_slots = 0
		}
		add_building_slot = 1
		add_building = common_tradeport_01
	}
	else_if = {
		limit = {
			NOR = {
				has_building = common_tradeport_01
				has_building = common_tradeport_02
				has_building = common_tradeport_03
				has_building = common_tradeport_04
				has_building = common_tradeport_05
				has_building = common_tradeport_06
				has_building = common_tradeport_07
				has_building = common_tradeport_08
			}
			free_building_slots > 1
		}
		add_building = common_tradeport_01
	}
	else_if = {
		limit = {
			has_building = common_tradeport_01
		}
		add_building = common_tradeport_02
	}
	else_if = {
		limit = {
			has_building = common_tradeport_02
		}
		add_building = common_tradeport_03
	}
	else_if = {
		limit = {
			has_building = common_tradeport_03
		}
		add_building = common_tradeport_04
	}
	else_if = {
		limit = {
			has_building = common_tradeport_04
		}
		add_building = common_tradeport_05
	}
	else_if = {
		limit = {
			has_building = common_tradeport_05
		}
		add_building = common_tradeport_06
	}
	else_if = {
		limit = {
			has_building = common_tradeport_06
		}
		add_building = common_tradeport_07
	}
	else_if = {
		limit = {
			has_building = common_tradeport_07
		}
		add_building = common_tradeport_08
	}
}

# Upgrades a single guidhall building
# If such a building does not exist and can be built, it is built. If no slot exists, a slot is added and it is built.
upgrade_guildhall = {
	# not a thing in BA
}
