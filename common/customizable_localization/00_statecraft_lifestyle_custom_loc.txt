﻿

StatecraftGetVassalType = {
	type = character

	text = {
		trigger = { has_government = feudal_government}
		localization_key = statecraft_ongoing.4010.nobility
		fallback = yes
	}
	text = {
		trigger = { has_government = tribal_government}
		localization_key = statecraft_ongoing.4010.chieftains
	}
	text = {
		trigger = { has_government = theocracy_government}
		localization_key = statecraft_ongoing.4010.clergy
	}
}
