﻿
GetRegionalArcherMAA = {
	type = character
	
	#text = {
	#	trigger = { culture = { has_cultural_parameter = unlock_maa_archers_of_the_nile } }
	#	localization_key = archers_of_the_nile
	#}
	text = {
		trigger = { always = no }
		fallback = yes
		localization_key = bowmen
	}
}

GetRegionalInfantryMAA = {
	type = character
	random_valid = yes
	
	text = {
		trigger = { always = no }
		fallback = yes
		localization_key = infantry
	}
}

GetRegionalCavalryMAA = {
	type = character
	random_valid = yes
	
	text = {
		trigger = { always = no }
		fallback = yes
		localization_key = cavalry
	}
}

GetRegionalSkirmisherMAA = {
	type = character
	random_valid = yes
	
	text = {
		trigger = { always = no }
		fallback = yes
		localization_key = light_footmen
	}
}