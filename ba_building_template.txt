﻿
template123_01 = {
	construction_time = standard_construction_time

	can_construct_potential = {
		building_requirement_castle_city_church = { LEVEL = 01 }
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}

	cost_gold = normal_building_tier_1_cost
	
	province_modifier = {
	}
	county_modifier = { 
	}
	
	next_building = template123_02

	type_icon = "icon_building_library.dds"
	
	ai_value = { 
	}
}

template123_02 = {
	construction_time = standard_construction_time

	can_construct_potential = {
		building_requirement_castle_city_church = { LEVEL = 01 } 
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}

	cost_gold = normal_building_tier_2_cost
	
	province_modifier = { 
	}
	county_modifier = { 
	}
	
	next_building = template123_03
	ai_value = { 
	}
}

template123_03 = {
	construction_time = standard_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 02 }
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}

	cost_gold = normal_building_tier_3_cost
	
	province_modifier = { 
	}
	county_modifier = { 
	}
	
	next_building = template123_04
	ai_value = { 
	}
}

template123_04 = {
	construction_time = standard_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 02 }
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}

	cost_gold = normal_building_tier_4_cost
	
	province_modifier = { 
	}
	county_modifier = { 
	}
	
	next_building = template123_05
	ai_value = { 
	}
}

template123_05 = {
	construction_time = standard_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 03 }
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}

	cost_gold = normal_building_tier_5_cost
	
	province_modifier = { 
	}
	county_modifier = { 
	}
	
	next_building = template123_06
	ai_value = { 
	}
}

template123_06 = {
	construction_time = standard_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 03 }
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}

	cost_gold = normal_building_tier_6_cost
	
	province_modifier = { 
	}
	county_modifier = { 
	}
	
	next_building = template123_07
	ai_value = { 
	}
}

template123_07 = {
	construction_time = standard_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 04 }

	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}

	cost_gold = normal_building_tier_7_cost
	
	province_modifier = {
		
	}
	county_modifier = {
		
	}
	
	next_building = template123_08
	ai_value = {
	}
}

template123_08 = {
	construction_time = standard_construction_time

	can_construct = {
		building_requirement_castle_city_church = { LEVEL = 04 }
	}

	can_construct_showing_failures_only = {
		building_requirement_tribal = no
	}
	
	cost_gold = normal_building_tier_8_cost
	
	province_modifier = {
		
	}
	county_modifier = {
		
	}
	
	ai_value = {
	}
}