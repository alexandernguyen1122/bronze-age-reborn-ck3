island_region_karpathos = {
	duchies = {	d_karpathos }
}
island_region_astypalea = {
	counties = { c_astypalea }
}
island_region_kaudos = {
	counties = { c_kaudos }
}
island_region_antikythera = {
	counties = { c_antikythera }
}
island_region_halonnesos = {
	counties = { c_halonnesos }
}
island_region_psyra = {
	counties = { c_psyra }
}
island_region_crete = {
	duchies = {
		d_kissamos
		d_kydonia
		d_knossos
		d_phaistos
		d_zakros
	}
	counties = {
		c_astale c_eleutherna c_korion c_armenoi
	}
}
island_region_cyprus = {
	duchies = {
		d_kouklia
		d_amorosa
		d_kalabasos
		d_alasiya
		d_ledra
		d_philia
		d_vounous
		d_rialonsa
	}
}
island_region_kythera = {
	counties = { c_kythera }
}
island_region_zakynthos = {
	counties = { c_zakynthos }
}
island_region_kefalonia = {
	counties = { 
		c_sami
		c_kefalonia
		c_ithaka 
	}
}
island_region_kerkyra = {
	duchies = { d_kerkyra }
}
island_region_melos = {
	counties = { c_melos }
}
island_region_siphnos = {
	counties = { c_siphnos }
}
island_region_seriphos = {
	counties = { c_seriphos }
}
island_region_kythnos = {
	counties = { c_kythnos }
}
island_region_keos = {
	counties = { c_keos }
}
island_region_syros = {
	counties = { c_syros }
}
island_region_mykonos = {
	counties = { c_mykonos }
}
island_region_naxos = {
	counties = { c_naxos }
}
island_region_amorgos = {
	counties = { c_amorgos }
}
island_region_thera = {
	counties = { c_thera }
}
island_region_sikinos = {
	counties = { c_sikinos }
}
island_region_ios = {
	counties = { c_ios }
}
island_region_folegandros = {
	counties = { c_folegandros }
}
island_region_ikaros = {
	counties = { c_ikaros }
}
island_region_korsiai = {
	counties = { c_korsiai }
}
island_region_patmos = {
	counties = { c_patmos }
}
island_region_lepsia = {
	counties = { c_lepsia }
}
island_region_leroskalimnos = {
	counties = { 
		c_leros
		c_kalimnos
	}
}
island_region_kos = {
	counties = { c_kos }
}
island_region_nisyros = {
	counties = { c_nisyros }
}
island_region_telos = {
	counties = { c_telos }
}
island_region_symi = {
	counties = { c_symi }
}
island_region_lezpa = {
	duchies = { d_lezpa }
}
island_region_dolopes = {
	counties = { c_dolopes }
}
island_region_lemna = {
	counties = { c_poliochne }
}
island_region_imbros = {
	counties = { c_imbros }
}
island_region_samothrake = {
	counties = { c_samothrake }
}
island_region_rhodes = {
	duchies = { 
		d_ialyssos
		d_lindos
	}
}
island_region_aigina = {
	counties = { c_aigina }
}
island_region_dilmun = {
	counties = { c_dilmun }
}
island_region_kharg = {
	counties = { c_kharg }
}
island_region_arakata = {
	counties = { c_arakata }
}
island_region_forur = {
	counties = { c_forur }
}
island_region_sirri = {
	counties = { c_sirri }
}
island_region_moussa = {
	counties = { c_moussa }
}
island_region_nuayr = {
	counties = { c_nuayr }
}
island_region_zirkuh = {
	counties = { c_zirkuh }
}
island_region_das = {
	counties = { c_das }
}
island_region_delma = {
	counties = { c_delma }
}
island_region_tinosandros = {
	counties = { 
		c_gaurion
		c_andros
		c_tinos
	}
}
island_region_tenedos = {
	counties = { c_tenedos }
}